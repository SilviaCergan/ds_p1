/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.fils.dsproject.server.model.Person;
import ro.fils.dsproject.server.model.Student;
import ro.fils.dsproject.server.repository.StudentRepository;

/**
 *
 * @author Silvia
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepository studentRepo;

    @Override
    public Student createNew(Student s) {
        return studentRepo.save(s);
    }

    @Override
    public boolean exists(Student s) {
        return studentRepo.equals(s);
    }

    @Override
    public void update(Student s) {
        studentRepo.save(s);
    }

    @Override
    public void delete(Student s) {
        studentRepo.delete(s);
    }

    @Override
    public boolean findbyPersonAndPassword(Person p, String pass) {
        List<Student> list = studentRepo.findbyPersonAndPassword(p, pass);
        return !list.isEmpty();
    }
    
    @Override
    public List<Student> findAll(){
        return studentRepo.findAll();
    }

    //TODO
    /**
     * See why it doesn't work affects      {@link ro.fils.dsproject.server.Server#setPrize}
     * {@link ro.fils.dsproject.server.Server#setAdress}
     * {@link ro.fils.dsproject.server.Server#attend}
     * {@link ro.fils.dsproject.server.repository.StudentRepository#getByPerson}
     * returns null
     *
     * @param p
     * @return
     */
    @Override
    public Student getByPerson(Person p) {
        Student std = studentRepo.getByPerson(p);
        if (std != null) {
            System.out.println("Student=============: " + std.toString());
            return std;
        }
        System.out.println("Student=============: E null in pizda masii");
        return null;
    }

    @Override
    public List<Student> findAllOrderedByGrade() {
        List<Student> students = null;
        //TODO Implement Repository Logic
        return students;
    }
}
