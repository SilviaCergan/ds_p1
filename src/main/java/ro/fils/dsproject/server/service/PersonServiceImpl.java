/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.fils.dsproject.server.model.Person;
import ro.fils.dsproject.server.repository.PersonRepository;

/**
 *
 * @author Silvia
 */
@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    PersonRepository personRepo;

    @Override
    public Person createNew(Person p) {
        return personRepo.save(p);
    }

    @Override
    public boolean exists(Person p) {
        return personRepo.equals(p);
    }

    @Override
    public void update(Person p) {
        personRepo.save(p);
    }

    @Override
    public void delete(Person p) {
        personRepo.delete(p);
    }

    @Override
    public Person findBySSN(String ssn) {
        List<Person> lst = personRepo.findbySSN(ssn);
        if (!lst.isEmpty()) {
            return lst.get(0);
        }
        return null;
    }

}
