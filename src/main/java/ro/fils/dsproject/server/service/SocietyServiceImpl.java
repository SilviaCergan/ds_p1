/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.fils.dsproject.server.model.Society;
import ro.fils.dsproject.server.repository.SocietyRepository;

/**
 *
 * @author Silvia
 */
@Service
public class SocietyServiceImpl implements SocietyService {
    
    @Autowired
    SocietyRepository societyRepo;
    
    @Override
    public Society createNew(Society s) {
        return societyRepo.save(s);
    }
    
    @Override
    public boolean exists(Society s) {
        return societyRepo.equals(s);
    }
    
    @Override
    public void update(Society s) {
        societyRepo.save(s);
    }
    
    @Override
    public void delete(Society s) {
        societyRepo.delete(s);
    }

    @Override
    public int getMembers(Society s) {
        List<Society> findbyName = societyRepo.findbyName(s.getName());
        if (!findbyName.isEmpty()) {
            Society get = findbyName.get(0);
           return get.getMembers().size();
        } 
        
        return 0;
    }

    @Override
    public Society findbyName(String name) {
        List<Society> findbyName = societyRepo.findbyName(name);
        if (!findbyName.isEmpty()) {
            return findbyName.get(0);
        } 
        
        return null;
    }

    
    
}
