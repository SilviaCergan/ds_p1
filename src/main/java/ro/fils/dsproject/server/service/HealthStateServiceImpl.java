/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.fils.dsproject.server.model.HealthState;
import ro.fils.dsproject.server.model.Person;
import ro.fils.dsproject.server.repository.HealthStateRepository;

/**
 *
 * @author Silvia
 */
@Service
public class HealthStateServiceImpl implements HealthStateService {

    @Autowired
    HealthStateRepository hsRepo;

    @Override
    public HealthState createNew(HealthState hs) {
        HealthState savedHs = hsRepo.save(hs);
        return savedHs;
    }

    @Override
    public boolean exists(HealthState hs) {
        return hsRepo.equals(hs);
    }

    @Override
    public void update(HealthState hs) {
        hsRepo.save(hs);
    }

    @Override
    public void delete(HealthState hs) {
        hsRepo.delete(hs);
    }

    @Override
    public List<HealthState> getAll() {
        return hsRepo.findAll();
    }

    @Override
    public HealthState findbyPerson(Person p) {
        List<HealthState> lst = hsRepo.findbyPerson(p);
        if (!lst.isEmpty()) {
            return lst.get(0);
        }
        return null;
    }

}
