/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.service;

import ro.fils.dsproject.server.model.Person;

/**
 *
 * @author Silvia
 */
public interface PersonService {

    public Person createNew(Person p);

    public boolean exists(Person p);

    public void update(Person p);

    public void delete(Person p);
    
    public Person findBySSN(String ssn);
}
