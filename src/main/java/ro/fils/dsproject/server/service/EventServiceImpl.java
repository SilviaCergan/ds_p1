/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.service;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.fils.dsproject.server.model.Event;
import ro.fils.dsproject.server.repository.EventRepository;

/**
 *
 * @author andre
 */
@Service
@Transactional
public class EventServiceImpl implements EventService {

    @Autowired
    EventRepository eventRepo;

    @Override
    public List<Event> findAll() {
        return eventRepo.findAll();
    }

    @Override
    public String findAllEventsAsString() {
        ArrayList<Event> list = (ArrayList<Event>) findAll();
        String s = null;
        for(Event e : list){
            s = s + e.getEventNameAndLocation() + " ";
        }
        return s;
    }

    @Override
    public Event findByEventName(String name) {
       return eventRepo.findByEventName(name);
    }

    @Override
    public void save(Event event){
        eventRepo.save(event);
    }
}
