/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.service;

import java.util.List;
import ro.fils.dsproject.server.model.Event;

/**
 *
 * @author andre
 */
public interface EventService {
    
    List<Event> findAll();
    
    String findAllEventsAsString();
    
    Event findByEventName(String name);
    
    void save(Event e);
}
