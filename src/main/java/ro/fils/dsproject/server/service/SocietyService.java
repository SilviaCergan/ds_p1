/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.service;

import ro.fils.dsproject.server.model.Society;

/**
 *
 * @author Silvia
 */
public interface SocietyService {

    public Society createNew(Society s);

    public boolean exists(Society s);

    public void update(Society s);

    public void delete(Society s);
    
    public int getMembers(Society s);
    
    public Society findbyName(String name);
}
