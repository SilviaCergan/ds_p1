/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.service;

import java.util.List;
import ro.fils.dsproject.server.model.HealthState;
import ro.fils.dsproject.server.model.Person;

/**
 *
 * @author Silvia
 */
public interface HealthStateService {

    public HealthState createNew(HealthState hs);

    public boolean exists(HealthState hs);

    public void update(HealthState hs);

    public void delete(HealthState hs);
    
    public List<HealthState> getAll();
    
    public HealthState findbyPerson(Person p);
}
