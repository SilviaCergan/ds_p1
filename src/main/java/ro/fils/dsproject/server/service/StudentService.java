/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.service;

import java.util.List;
import ro.fils.dsproject.server.model.Person;
import ro.fils.dsproject.server.model.Student;

/**
 *
 * @author Silvia
 */
public interface StudentService {

    public Student createNew(Student s);

    public boolean exists(Student s);

    public void update(Student s);

    public void delete(Student s);

    public boolean findbyPersonAndPassword(Person p, String pass);
    
    public Student getByPerson(Person p);
    
    public List<Student> findAllOrderedByGrade();
    
    public List<Student> findAll();
}
