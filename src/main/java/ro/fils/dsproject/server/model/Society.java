/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.model;

import java.io.Serializable;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.stereotype.Component;
import ro.fils.dsproject.server.GeneralHealthSystem;
import sun.security.util.Password;

/**
 *
 * @author Silvia
 */
@Entity
@Table(name = "society")
public class Society implements Serializable {

    private final int MAX_NEW_MEMBERS = 40;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Person> members;

    private String name;

    @Id
    @GeneratedValue
    private Long id;

    @Transient
    private static final Logger LOG = Logger.getLogger(Society.class.getName());

    public Society() {
    }

    public synchronized boolean addNewMember(Person e, String password) {

        if (members.size() < MAX_NEW_MEMBERS) {
            GeneralHealthSystem healthSystem = GeneralHealthSystem.getInstance();
            HealthState personRecord = healthSystem.getPersonRecord(e.getSSN());
            if (personRecord != null) {
                System.out.println("Society System getting health records: "+ personRecord.toString());
            }
            return members.add(e);
        }
        return false;
    }

    public Set<Person> getMembers() {
        return members;
    }

    public void setMembers(Set<Person> members) {
        this.members = members;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Society{" + "MAX_NEW_MEMBERS=" + MAX_NEW_MEMBERS + ", members=" + members + ", name=" + name + ", id=" + id + '}';
    }

}
