/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server;

import ro.fils.dsproject.server.model.Event;
import ro.fils.dsproject.server.model.Person;
import ro.fils.dsproject.server.model.Society;
import ro.fils.dsproject.server.model.Student;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ro.fils.dsproject.server.service.PersonService;
import ro.fils.dsproject.server.service.SocietyService;
import ro.fils.dsproject.server.service.EventService;
import ro.fils.dsproject.server.service.StudentService;

/**
 *
 * @author andre
 */
public class Server extends javax.swing.JFrame {

    DatagramSocket ds;
    DatagramPacket dp;
    String str, methodName, result;
    int prizeId, eventId;
    String ssn, password, replyMessage, events;
    String prizes = "Cal Mercedes Unicorn CDManele Lant Harpa Zana AbonamentRATB ConcertSalam Nimic";
    String prizeName, eventName, address;

    ClassPathXmlApplicationContext ctx;
    EventService eventService;
    StudentService studentService;
    Society societySystem;
    PersonService personService;
    SocietyService societyService;

    DefaultListModel<Student> studentsModel = new DefaultListModel();
    DefaultListModel<String> prizesModel = new DefaultListModel();

    public Server(ClassPathXmlApplicationContext ctx) {
        this.ctx = ctx;
        initComponents();
        eventService = ctx.getBean(EventService.class);
        studentService = ctx.getBean(StudentService.class);
        personService = ctx.getBean(PersonService.class);
        societyService = ctx.getBean(SocietyService.class);
        societySystem = societyService.findbyName("SecretSociety");

        ArrayList<Student> studentsList = (ArrayList<Student>) populateListStudents();
        ArrayList<String> prizesList = (ArrayList<String>) populatePrizesMap();

        studentsList.forEach(s -> studentsModel.addElement(s));
        prizesList.forEach(p -> prizesModel.addElement(p));

        listViewStudentsForGrades.setModel(studentsModel);
        listViewPrizesForMembers.setModel(prizesModel);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        listViewPrizesForMembers = new javax.swing.JList<>();
        labelPrizesList = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listViewStudentsForGrades = new javax.swing.JList<>();
        editStudentGrade = new javax.swing.JTextField();
        btnAddGradeForStudent = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        listViewPrizesForMembers.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(listViewPrizesForMembers);

        labelPrizesList.setText("Prizes List");

        jLabel1.setText("Students List");

        listViewStudentsForGrades.setModel(new DefaultListModel());
        jScrollPane2.setViewportView(listViewStudentsForGrades);

        btnAddGradeForStudent.setText("Set Grade");
        btnAddGradeForStudent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddGradeForStudentActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelPrizesList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(editStudentGrade, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAddGradeForStudent, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelPrizesList)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editStudentGrade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddGradeForStudent)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddGradeForStudentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddGradeForStudentActionPerformed
        setGrade();
        JOptionPane.showMessageDialog(null, "Grade set");
    }//GEN-LAST:event_btnAddGradeForStudentActionPerformed

    private boolean login(String ssn, String password) {
        Person person = personService.findBySSN(ssn);
        return studentService.findbyPersonAndPassword(person, password);
    }

    private boolean apply(String ssn, String password) {
        boolean addNewMember = societySystem.addNewMember(personService.findBySSN(ssn), password);
        if (addNewMember) {
            Student newStudent = new Student();
            newStudent.setPerson(personService.findBySSN(ssn));
            newStudent.setPassword(password);
            studentService.createNew(newStudent);
            societyService.update(societySystem);
            return true;
        } else {
            return false;
        }
    }

    private boolean setPrize(String ssn, String prizeName) {
        Student s = studentService.getByPerson(personService.findBySSN(ssn));
        s.setDesiredPrize(prizeName);
        studentService.update(s);
        return true;
    }

    private boolean attend(String ssn, String eventName) {
        Student s = studentService.getByPerson(personService.findBySSN(ssn));
        Event e = eventService.findByEventName(eventName);
        e.addStudentToEvent(s);
        eventService.save(e);
        return true;
    }

    private String message(String message) {
        String reply = JOptionPane.showInputDialog(message);
        return reply;
    }

    private boolean setAddress(String ssn, String address) {
        Student s = studentService.getByPerson(personService.findBySSN(ssn));
        s.setShippingAddress(address);
        studentService.update(s);
        return true;
    }

    private boolean setGrade() {
        String ssnGrade = listViewStudentsForGrades.getSelectedValue().getPerson().getSSN();
        Student s = studentService.getByPerson(personService.findBySSN(ssnGrade));
        s.setGrade(Integer.parseInt(editStudentGrade.getText()));
        studentService.update(s);
        return false;
    }

    public void startServer() {
        Runnable serverTask = () -> {
            try {
                ds = new DatagramSocket(1200);
                byte b[] = new byte[40960];
                while (true) {
                    dp = new DatagramPacket(b, b.length);
                    ds.receive(dp);
                    str = new String(dp.getData(), 0, dp.getLength());
                    if (str.equalsIgnoreCase("q")) {
                        System.exit(1);
                    } else {
                        StringTokenizer st = new StringTokenizer(str, " ");
                        int i = 0;
                        while (st.hasMoreTokens()) {

                            String token = st.nextToken();
                            methodName = token;
                            if (token.equalsIgnoreCase("login")) {
                                ssn = st.nextToken();
                                password = st.nextToken();
                            } else if (token.equalsIgnoreCase("apply")) {
                                ssn = st.nextToken();
                                password = st.nextToken();
                            } else if (token.equalsIgnoreCase("setPrize")) {
                                ssn = st.nextToken();
                                prizeName = st.nextToken();
                            } else if (token.equalsIgnoreCase("attend")) {
                                ssn = st.nextToken();
                                eventName = st.nextToken();
                            } else if (token.equalsIgnoreCase("message")) {
                                String messageFromStudent = null;
                                while (st.hasMoreTokens()) {
                                    messageFromStudent = messageFromStudent + st.nextToken() + " ";
                                }
                                result = "message " + message(messageFromStudent);
                            } else if (token.equalsIgnoreCase("populatePrizes")) {

                            } else if (token.equalsIgnoreCase("populateEvents")) {
                                events = eventService.findAllEventsAsString();
                            } else if (token.equalsIgnoreCase("setAddress")) {
                                ssn = st.nextToken();
                                while (st.hasMoreTokens()) {
                                    address += st.nextToken();
                                }
                                System.out.println(ssn + " " + address);
                            }
                        }
                    }
                    InetAddress ia = InetAddress.getLocalHost();
                    if (methodName.equalsIgnoreCase("login")) {
                        result = "login " + login(ssn, password);
                    }
                    if (methodName.equalsIgnoreCase("apply")) {
                        result = "apply " + apply(ssn, password);
                    }
                    if (methodName.equalsIgnoreCase("setPrize")) {
                        result = "setPrize " + setPrize(ssn, prizeName);
                    }
                    if (methodName.equalsIgnoreCase("attend")) {
                        result = "attend " + attend(ssn, eventName);
                    }
                    if (methodName.equalsIgnoreCase("populatePrizes")) {
                        result = "populatePrizes " + prizes;
                    }
                    if (methodName.equalsIgnoreCase("populateEvents")) {
                        result = "populateEvents " + events;
                    }
                    if (methodName.equalsIgnoreCase("setAddress")) {
                        result = "setAddress " + setAddress(ssn, address);
                    }

                    byte b1[] = result.getBytes();
                    DatagramSocket ds1 = new DatagramSocket();
                    DatagramPacket dp1 = new DatagramPacket(b1, b1.length,
                            InetAddress.getLocalHost(), 1300);
                    System.out.println("result : " + result + "\n");
                    ds1.send(dp1);
                }
            } catch (IOException | NumberFormatException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();
    }

    public Map<String, String> computePrizeMap() {
        Map<String, String> prizeMap = new HashMap<>();
        String[] splitPrizes = prizes.split(" ");
        ArrayList<String> prizesList = new ArrayList<>();
        prizesList.addAll(Arrays.asList(splitPrizes));
        for (Student s : studentService.findAll()) {
            if (s.getDesiredPrize() != null) {
                if (prizesList.contains(s.getDesiredPrize())) {
                    prizeMap.put(s.getPerson().getName(), s.getDesiredPrize());
                    prizesList.remove(s.getDesiredPrize());
                    prizeMap.put(s.getPerson().getName(), prizesList.remove(0));
                }
            }else{
                prizeMap.put(s.getPerson().getName(), prizesList.remove(0));
            }
        }
        return prizeMap;
    }

    public final List<String> populatePrizesMap() {
        Map<String, String> map = computePrizeMap();
        List<String> list = new ArrayList<>();
        map.entrySet().stream().forEach((e) -> {
            list.add(e.getKey() + ", " + e.getValue());
        });
        return list;
    }

    public final List<Student> populateListStudents() {
        ArrayList<Student> students = (ArrayList<Student>) studentService.findAll();
        return students;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddGradeForStudent;
    private javax.swing.JTextField editStudentGrade;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelPrizesList;
    private javax.swing.JList<String> listViewPrizesForMembers;
    private javax.swing.JList<Student> listViewStudentsForGrades;
    // End of variables declaration//GEN-END:variables
}
