/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.fils.dsproject.server.model.Person;
import ro.fils.dsproject.server.model.Student;

/**
 *
 * @author Silvia
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{
    
    @Query("select s from Student s where s.person = ?1 and s.password = ?2")
    List<Student> findbyPersonAndPassword(Person p, String pass);
    
    @Query("select s from Student s where s.person = ?1")
    Student getByPerson(Person p);

}
