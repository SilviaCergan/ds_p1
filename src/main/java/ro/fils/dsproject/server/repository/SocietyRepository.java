/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.fils.dsproject.server.model.Society;

/**
 *
 * @author Silvia
 */
@Repository
public interface SocietyRepository extends JpaRepository<Society, Long>{
    @Query("select s from Society s where s.name = ?1")
    List<Society> findbyName(String name);
}
