/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.fils.dsproject.server.model.HealthState;
import ro.fils.dsproject.server.model.Person;

/**
 *
 * @author Silvia
 */
@Repository
public interface HealthStateRepository extends JpaRepository<HealthState, Long> {
    
    @Query("select h from HealthState h where h.person = ?1")
    List<HealthState> findbyPerson(Person p);
}
