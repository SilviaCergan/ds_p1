/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.fils.dsproject.server.model.Person;

/**
 *
 * @author Silvia
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    
    @Query("select p from Person p where p.SSN = ?1")
    List<Person> findbySSN(String ssn);
}
