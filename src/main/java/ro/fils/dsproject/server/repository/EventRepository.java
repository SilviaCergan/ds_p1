/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.fils.dsproject.server.model.Event;

/**
 *
 * @author andre
 */
@Repository
public interface EventRepository extends JpaRepository<Event, Long>{
    
    @Query("SELECT e FROM Event e WHERE e.eventNameAndLocation = :name")
    Event findByEventName(@Param("name") String name);
}
