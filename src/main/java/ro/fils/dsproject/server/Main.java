/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Silvia
 */
@SpringBootApplication
public class Main extends SpringApplication {

    static ClassPathXmlApplicationContext applicationContext;

    public static void main(String[] args) {
        applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        Server s = new Server(applicationContext);
        s.setVisible(true);
        s.startServer();
//            
//        // Test DATA:
//            
//            
//        PersonService personService = applicationContext.getBean(PersonService.class);
//        StudentService studentService = applicationContext.getBean(StudentService.class);
//        HealthStateService hsService = applicationContext.getBean(HealthStateService.class);
//        SocietyService societyService = applicationContext.getBean(SocietyService.class);
//
//        Person p1 = new Person();
//        p1.setName("Matei");
//        p1.setSurname("Popa");
//        p1.setSSN("1900209042952");
//        p1.setAge(21);
//        p1.setEmail("popa.matei@gmail.com");
//        p1.setPhoneNo("0745995129");
//
//        personService.createNew(p1);
//
//        Student s1 = new Student();
//        s1.setPerson(personService.findBySSN("1900209042952"));
//        s1.setUniversity("Politehnica");
//        s1.setFieldOfStudy("computer science");
//        s1.setYearOfStudy("4");
//
//        studentService.createNew(s1);
//        HealthState hs = new HealthState();
//        hs.setPerson(personService.findBySSN("1900209042952"));
//        hs.setName("Health Record");
//        hs.setDescription("no health issues");
//
//        hsService.createNew(hs);
//        
//        Society so1 = new Society();
//        so1.setName("Society1");
//        societyService.createNew(so1);
//
    }

}
