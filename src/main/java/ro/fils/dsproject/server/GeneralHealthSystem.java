/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.server;

import ro.fils.dsproject.server.model.Person;
import ro.fils.dsproject.server.model.HealthState;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ro.fils.dsproject.server.service.HealthStateService;
import ro.fils.dsproject.server.service.PersonService;

/**
 *
 * @author Silvia
 */
public class GeneralHealthSystem {

    ConfigurableApplicationContext applicationContext; 
    HealthStateService hsService;
    PersonService personService;

    private static final GeneralHealthSystem instance = new GeneralHealthSystem();

    private GeneralHealthSystem() {
        this.applicationContext =  new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        this.hsService = applicationContext.getBean(HealthStateService.class);
        this.personService = applicationContext.getBean(PersonService.class);
    }

    public static GeneralHealthSystem getInstance() {
        return instance;
    }

    // needs thread safety
    public synchronized boolean addPersonToHealthSystem(HealthState personHS) {
        HealthState createNew = hsService.createNew(personHS);
        return createNew != null;
    }

    // needs thread safety
    public synchronized HealthState getPersonRecord(String ssn) {
        
        Person person = personService.findBySSN(ssn);
        if (person != null) {
            return hsService.findbyPerson(person);
        }
        return null;  
    }
    
    // no need for thread safety
    private boolean ssnMatch(Person p, String ssn) {
        return p.getSSN().equals(ssn);
    }
    
}
