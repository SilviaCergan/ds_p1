/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.fils.dsproject.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author andre
 */
public class DashboardClientFrame extends javax.swing.JFrame {

    InetAddress ia;
    DatagramSocket ds;
    DatagramSocket ds1;
    String stdSSN;
    DefaultListModel<String> eventsModel = new DefaultListModel();
    DefaultListModel<String> prizesModel = new DefaultListModel();

    public DashboardClientFrame(String stdSSN, InetAddress ia, DatagramSocket ds, DatagramSocket ds1) throws SocketException, UnknownHostException {
        initComponents();
        this.ia = ia;
        this.ds = ds;
        this.ds1 = ds1;
        this.stdSSN = stdSSN;
        ArrayList<String> eventsList = populateEventsList();
        ArrayList<String> prizesList = populatePrizesList();

        eventsList.forEach(e -> eventsModel.addElement(e));
        prizesList.forEach(p -> prizesModel.addElement(p));

        listViewEvents.setModel(eventsModel);
        listViewPrize.setModel(prizesModel);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listViewEvents = new javax.swing.JList<>();
        btnParticipate = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        listViewPrize = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnDesiredPrize = new javax.swing.JButton();
        editAddress = new javax.swing.JTextField();
        btnSetAddress = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        listViewEvents.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(listViewEvents);

        btnParticipate.setText("Participate");
        btnParticipate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnParticipateActionPerformed(evt);
            }
        });

        listViewPrize.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(listViewPrize);

        jLabel1.setText("Events");

        jLabel2.setText("Prizes");

        btnDesiredPrize.setText("Set Desired Prize");
        btnDesiredPrize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesiredPrizeActionPerformed(evt);
            }
        });

        btnSetAddress.setText("Set Address");
        btnSetAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSetAddressActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnParticipate, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDesiredPrize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 47, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(editAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSetAddress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnParticipate, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDesiredPrize, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editAddress)
                    .addComponent(btnSetAddress, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnParticipateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnParticipateActionPerformed
        try {
            String attendMessage = "attend " + stdSSN + " " + listViewEvents.getSelectedValue();
            byte b[] = attendMessage.getBytes();
            DatagramPacket dp = new DatagramPacket(b, b.length, ia, 1200);
            ds.send(dp);

            dp = new DatagramPacket(b, b.length);
            ds1.receive(dp);
            String s = new String(dp.getData(), 0, dp.getLength());
            if (s.startsWith("attend")) {
                if (s.contains("true")) {
                    JOptionPane.showMessageDialog(null, "You have succesfully registered for the event");
                } else {
                    JOptionPane.showMessageDialog(null, "Ups, something went wrong. Try again!");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LoginRegisterClientFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnParticipateActionPerformed

    private void btnDesiredPrizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesiredPrizeActionPerformed
        try {
            String attendMessage = "setPrize " + stdSSN + " " + listViewPrize.getSelectedValue();
            System.out.println(attendMessage);
            byte b[] = attendMessage.getBytes();
            DatagramPacket dp = new DatagramPacket(b, b.length, ia, 1200);
            ds.send(dp);

            dp = new DatagramPacket(b, b.length);
            ds1.receive(dp);
            String s = new String(dp.getData(), 0, dp.getLength());
            if (s.startsWith("setPrize")) {
                if (s.contains("true")) {
                    JOptionPane.showMessageDialog(null, "You have succesfully set your prize");
                } else {
                    JOptionPane.showMessageDialog(null, "Ups, something went wrong. Try again!");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LoginRegisterClientFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnDesiredPrizeActionPerformed

    private void btnSetAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSetAddressActionPerformed
        try {
            String attendMessage = "setAddress " + stdSSN + " " + editAddress.getText();
            byte b[] = attendMessage.getBytes();
            DatagramPacket dp = new DatagramPacket(b, b.length, ia, 1200);
            ds.send(dp);

            dp = new DatagramPacket(b, b.length);
            ds1.receive(dp);
            String s = new String(dp.getData(), 0, dp.getLength());
            if (s.startsWith("setAddress")) {
                if (s.contains("true")) {
                    JOptionPane.showMessageDialog(null, "You have succesfully set your address");
                } else {
                    JOptionPane.showMessageDialog(null, "Ups, something went wrong. Try again!");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LoginRegisterClientFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSetAddressActionPerformed

    private ArrayList<String> populatePrizesList() {
        ArrayList<String> result = new ArrayList<>();
        try {
            String attendMessage = "populatePrizes";
            byte b[] = attendMessage.getBytes();
            DatagramPacket dp = new DatagramPacket(b, b.length, ia, 1200);
            ds.send(dp);

            byte b2[] = new byte[40960];
            dp = new DatagramPacket(b2, b2.length);
            ds1.receive(dp);
            String s = new String(dp.getData(), 0, dp.getLength());
            String[] split = s.split(" ");
            System.out.println("prizes ++++++ " + s);
            for (String st : split) {
                if (!st.equals("populatePrizes") && st!= null) {
                    result.add(st);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LoginRegisterClientFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private ArrayList<String> populateEventsList() {
        ArrayList<String> result = new ArrayList<>();
        try {
            String attendMessage = "populateEvents";
            byte b[] = attendMessage.getBytes();
            DatagramPacket dp = new DatagramPacket(b, b.length, ia, 1200);
            ds.send(dp);

            byte b2[] = new byte[40960];
            dp = new DatagramPacket(b2, b2.length);
            ds1.receive(dp);
            String s = new String(dp.getData(), 0, dp.getLength());
            String[] split = s.split(" ");
            System.out.println("events ++++++ " + s);
            for (String st : split) {
                if (!st.equals("populateEvents") && s != null) {
                    result.add(st);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LoginRegisterClientFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDesiredPrize;
    private javax.swing.JButton btnParticipate;
    private javax.swing.JButton btnSetAddress;
    private javax.swing.JTextField editAddress;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<String> listViewEvents;
    private javax.swing.JList<String> listViewPrize;
    // End of variables declaration//GEN-END:variables
}
