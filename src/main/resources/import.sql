
CREATE TABLE IF NOT EXISTS `persons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `SSN` varchar(255) DEFAULT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phoneNo` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;


INSERT INTO `persons` (`id`, `SSN`, `age`, `email`, `name`, `phoneNo`, `surname`) VALUES
(1, '1900209042952', 21, 'popa.matei@gmail.com', 'Matei', '0745995129', 'Popa'),
(2, '1910624175364', 24, 'popa.ion@gmail.com', 'Ion', '0745995129', 'Popa'),
(3, '2911006180691', 24, 'pop.alina@gmail.com', 'Alina', '0745995129', 'Pop'),



CREATE TABLE IF NOT EXISTS `healthstates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `others` varchar(255) DEFAULT NULL,
  `person_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tocgq4admgrkdof7pggm97bix` (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;


INSERT INTO `healthstates` (`id`, `description`, `name`, `others`, `person_id`) VALUES
(1, 'no health issues', 'Health Record', NULL, 1),
(2, 'no health issues', 'Health Record', NULL, 2),
(3, 'no health issues', 'Health Record', NULL, 3);


ALTER TABLE `healthstates`
  ADD CONSTRAINT `FK_tocgq4admgrkdof7pggm97bix` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`);


CREATE TABLE IF NOT EXISTS `society` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `MAX_NEW_MEMBERS` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


INSERT INTO `society` (`id`, `MAX_NEW_MEMBERS`, `name`) VALUES
(1, 40, 'Society1');
